boot = {
  x : 0,
	y : 256-16,
	a : 0,
	spr_player : [[1, -1, 2, -1, 3, -1, 4, -1],
                [0, 5, 0, 6, 0, 7, 0, 8],
                [9, 0, 10, 0, 11, 0, 12, 0],
	              [-1, 13, 0, 14, 0, 15, 0, -1],
	              [1, 0, 1, 0, 1, 0, 1, -1],
	              [0, 1, 0, 1, 0, 1, 0, 1],
	              [1, 0, 1, 0, 1, 0, 1, 0],
	              [-1, 1, -1, 1, 0, 1, -1, 1]],
	
  xs : [],
	ys : [],
	nb : 40,
	s : "\n/ ~ $ ",
	c : "",
	time : 0.0,
	
	init : function() {
	  print("init");
	  for (var i = 0; i < this.nb; i++) {
		  this.xs.push(64 + Math.cos((360/this.nb) * i) * 40 + 128);
		  this.ys.push(64 + Math.sin((360/this.nb) * i) * 40 + 128);
	  }
	},
  
	tick : function() {
		graphics.clear(0)
		if (keyboard.isKeyDown(27)) {
		    graphics.randomize();  
		}
		
		for (var i = 32; i <= 128; i++) {
			if(keyboard.isKeyPressed(i)) {
				this.s += String.fromCharCode(i);
				this.c += String.fromCharCode(i);
			}
		}
			
		if(keyboard.isKeyPressed(10)) {
			if (this.c.toUpperCase() == "EXIT") console.shutdown();
			if (this.c.toUpperCase() == "HELLO") {
				console.execScript("root/src/", "hello");
			} else if (this.c.toLowerCase().startsWith("cd ")) {
				var params = this.c.split(" ");
				if (filesystem.cd(params[1]) == "") {
					this.s+="\nErreur : le dossier n'existe pas";
				}
			} else if (this.c.toLowerCase().startsWith("run ")) {
				var params = this.c.split(" ");
				if (filesystem.fileExists(params[1])) 
					console.execScript(filesystem.pwd(), params[1]);
				else
					this.s+="\nErreur : le fichier n'existe pas"; 
			}
		    this.c = "";
		    this.s += "\n" + filesystem.pwd() + " ~ $ ";
		}
		
		if(keyboard.isKeyPressed(8)) {
		  var l = this.c.length;
		  this.c = this.c.slice(0, -1);
		  if (l != this.c.length) this.s = this.s.slice(0, -1);
		}
			
		this.xs = []
		this.ys = []
		this.time += 0.0001
		this.nb = Math.sin(this.time) * 5 + 10
		for (var i = 0; i < this.nb; i++) {
		  this.xs.push(Math.cos((360/this.nb) * i + this.time*-500) * (8 + Math.cos(this.time*2000)*5) + 256-16);
		  this.ys.push(Math.sin((360/this.nb) * i + this.time*-500) * (8 + Math.cos(this.time*2000)*5) + 256-16);
		}
		  
		this.a += 0.05;
	  
	  this.x = 256-16 + Math.cos(this.a) * 16;
	  this.y = 256-16 + Math.sin(this.a) * 16;  
		  
		for(var i = 0; i < this.xs.length; i++) {
		  graphics.line(this.xs[i], this.ys[i], this.x, this.y, Math.round(this.time*500)%15+1);
		}
		  
		graphics.cursor(0, 0);
		  
		graphics.text(this.s, 7);
	}
}
