hello = {
	init: function() {
		print("[INIT] hello.js");
	},
	
	tick: function() {
		graphics.clear(0);
		graphics.line(1, 2, 1, 63, 5);
		graphics.line(64, 2, 64, 63, 5);	
		graphics.line(2, 1, 63, 1, 5);
		graphics.line(2, 64, 63, 64, 5);
	}
}