import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Keyboard extends KeyAdapter {
	
	private Console console;
	
	boolean[] keys_down;
	boolean[] keys_down_last;
	
	public Keyboard(Console w) {
		
		this.console = w;
		console.addKeyListener(this);
		
		keys_down = new boolean[255];
		for (int i = 0; i < keys_down.length; i++) keys_down[i] = false;
		keys_down_last = new boolean[255];
		for (int i = 0; i < keys_down.length; i++) keys_down[i] = false;
	}
	
	/*
	 * call at the end of the gameloop
	 */
	public void update() {
		for (int i = 0; i < keys_down.length; i++) {
			keys_down_last[i] = keys_down[i];
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyChar() >= 255) return;
		keys_down[e.getKeyChar()] = false;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyChar() >= 255) return;
		keys_down[e.getKeyChar()] = true;
	}
	
	public boolean isKeyDown(int key) {
		return keys_down[key];
	}
	
	public boolean isKeyPressed(int key) {
		return keys_down[key] && !keys_down_last[key];
	}
	
	public boolean isKeyReleased(int key) {
		return !keys_down[key] && keys_down_last[key];
	}
}
