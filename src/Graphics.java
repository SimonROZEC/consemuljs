import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Random;

import javax.swing.JPanel;

public class Graphics extends JPanel {
	
	private int[] frame;
	private BufferedImage image;
	private Console console;
	Random r = new Random();
	
	private int text_cursor = 0;
	public final int TEXT_COLS, TEXT_LINES;
	
	public Graphics(Console w) {
		setPreferredSize(new Dimension(w.WIDTH * w.SCALE, w.HEIGHT * w.SCALE));
		image = new BufferedImage(w.WIDTH, w.HEIGHT, BufferedImage.TYPE_INT_RGB);
		TEXT_COLS = w.WIDTH / 8;
		TEXT_LINES = w.HEIGHT / 8;
		frame = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();
		console = w;
		
		console.add(this);
	}
	
	public void randomize() {
		for (int i = 0; i < frame.length; i++) {
			frame[i] = console.getPalette()[r.nextInt(console.getPalette().length)];
		}
	}
	
	public void clear(int color) {
		for (int i = 0; i < frame.length; i++) {
			frame[i] = console.getPalette()[color];
		}
	}
	
	public void point(int x, int y, int color) {
		int id = x + console.WIDTH * y;
		if (id < 0 || id >= frame.length || x < 0 || x >= console.WIDTH) return;
		frame[x + console.WIDTH * y] = console.getPalette()[color];
	}
	
	public void line(int x1, int y1, int x2, int y2, int color) {
		int d = 0;
		 
        int dx = Math.abs(x2 - x1);
        int dy = Math.abs(y2 - y1);
 
        int dx2 = 2 * dx; // slope scaling factors to
        int dy2 = 2 * dy; // avoid floating point
 
        int ix = x1 < x2 ? 1 : -1; // increment direction
        int iy = y1 < y2 ? 1 : -1;
 
        int x = x1;
        int y = y1;
 
        if (dx >= dy) {
            while (true) {
            	point(x, y, color);
                if (x == x2)
                    break;
                x += ix;
                d += dy2;
                if (d > dx) {
                    y += iy;
                    d -= dx2;
                }
            }
        } else {
            while (true) {
                point(x, y, color);
                if (y == y2)
                    break;
                y += iy;
                d += dx2;
                if (d > dy) {
                    x += ix;
                    d -= dy2;
                }
            }
        }
	}
	
	public void cursor(int x, int y) {
		if (x >= TEXT_COLS) x = TEXT_COLS - 1;
		if (y >= TEXT_LINES) y = TEXT_LINES - 1;
		if (x < 0) x = 0;
		if (y < 0) y = 0;
		text_cursor = y * TEXT_COLS + x;
	}
	
	public void cursor(int i) {
		text_cursor = i;
	}
	
	public int getCursorX() {
		return text_cursor % TEXT_COLS;
	}
	
	public int getCursorY() {
		return text_cursor / TEXT_COLS;
	}
	
	public void text(String str, int color) {
		
		int cx = getCursorX();
		
		long[] bitmaps = GraphicsUtils.stringToBitmaps(str);
		
		for (long b : bitmaps) {
			if (b == '\n') {
				text_cursor += TEXT_COLS - (getCursorX() - cx);
			} else {
				for (int i = 0; i < 64; i++) {
					byte pixel = (byte) (b >> (63-i) & 1);
					if (pixel != 0) {
					point(getCursorX()*8 + 8 - (i % 8), 
						  getCursorY()*8 + (i / 8),
						  color
						 );
					}
				}
				text_cursor++;
			}
		}
	}
	
	
	public synchronized void render() {
		getGraphics().drawImage(image, 0, 0, getWidth(), getHeight(), null);
	}
}
