package res;

import com.google.gson.annotations.SerializedName;

public class Sprite {
	
	@SerializedName(value = "data")
	private final int[] data;
	
	@SerializedName(value = "mask")
	private final long mask;
	
	public Sprite(int[] data, long mask) {
		this.data = data;
		this.mask = mask;
	}
	
	public int[] getData() {
		return data;
	}
	
	public long getMask() {
		return mask;
	}
}
