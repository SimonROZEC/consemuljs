import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileSystem {
	private File root;
	private File pwd;
	
	private File folders[];
	
	public FileSystem(String root) {
		this.root = new File(root);
		if (!this.root.exists()) this.root.mkdirs();
		pwd = this.root;

		folders = new File[]{
				new File(root + "/src"),
				new File(root + "/os"),
				new File(root + "/res")
		};
		
		for (File f : folders) {
			if (!f.exists()) {
				f.mkdir();
			}
		}
	}
	
	public String pwd() {
		return ((pwd != root) ? "root/": "") + pwd.getName() + "/";
	}
	
	public String cd(String folder) {
		if (folder.equals("/") || folder.equals("root")) {
			pwd = root;
		} else if (folder.equals("src")) {
			pwd = folders[0];
		} else if (folder.equals("os")) {
			pwd = folders[1];
		} else if (folder.equals("res")) {
			pwd = folders[2];
		} else if (folder.equals("..") && pwd != root) {
			pwd = root;
		} else {
			return "";
		}
		return folder;
	}
	
	public boolean fileExists(String fileName) {
		File file = new File(pwd.getPath(), fileName);
		if (file.exists()) {
			return true;
		} else {
			return new File(pwd.getPath(), fileName+".js").exists();
		}
	}
	
	public boolean createFile(String fileName){
		File newfile = new File(pwd.getPath(), fileName);
		try {
			return newfile.createNewFile();
		} catch (IOException e) {
			return false;
		}
	}
	
	public String readFile(String fileName) throws IOException {
		return new String(Files.readAllBytes(Paths.get(fileName)), StandardCharsets.UTF_8);
	}
	/*
	public String writeFile(String fileName) {
		
	}*/
}
