import java.io.IOException;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JFrame;

import jdk.nashorn.api.scripting.JSObject;
import res.Sprite;

import com.google.gson.GsonBuilder;
import com.google.gson.Gson;


public class Console extends JFrame implements Runnable {
	
	public final int WIDTH;
	public final int HEIGHT;
	public static final int SCALE = 2;
	private int[] palette;
	
	public Graphics graphics;
	public Keyboard keyboard;
	public FileSystem filesystem;
	
	private long lastTime;
	private int fpsCount = 0;
	
	Object program = null;
	
	String bootcode;
	ScriptEngineManager manager = new ScriptEngineManager(); 
    ScriptEngine js = manager.getEngineByName("Nashorn");
    Invocable iv;
    
    private final GsonBuilder builder = new GsonBuilder();
    private final Gson gson = builder.create();
    
	public Console(int w, int h, int[] palette) throws ScriptException, IOException, NoSuchMethodException {
		this.WIDTH = w;
		this.HEIGHT = h;
		this.palette = palette;
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		graphics = new Graphics(this);
		keyboard = new Keyboard(this);
		filesystem = new FileSystem("./root/");
		
		bootcode = filesystem.readFile("./root/os/boot.js");
		
		
		setVisible(true);
		pack();
		js.put("keyboard", keyboard);
		js.put("graphics", graphics);
		js.put("console", this);
		js.put("filesystem", filesystem);
	    iv = (Invocable) js;
	    
	    execScript("root/os/", "boot");
	    loadData("root/res/", "boot");
	}
	
	public void execScript(String path, String name) throws NoSuchMethodException, ScriptException, IOException {
		js.eval(filesystem.readFile(path + name + ".js"));
		program = js.get(name);
		System.out.println(program);
		iv.invokeMethod(program, "init");
	}
	
	/*
	 * Modifie la "palette" de ressources de la console
	 */
	public void loadData(String path, String name) throws ScriptException, IOException {
		String content = filesystem.readFile(path+name+".data.json");
	}
	
	public void exit() throws ScriptException {
		program = js.get("boot");
	}
	
	public void run() {
		lastTime = System.currentTimeMillis();
		long st;
		while (true) {
			st = System.currentTimeMillis();
			if (keyboard.isKeyDown(27)) {
				try {
					this.exit();
					System.out.println("[ESCAPE]");
				} catch (ScriptException e) {
					e.printStackTrace();
				}
			}
			
			
			try {
				iv.invokeMethod(program, "tick");
			} catch (NoSuchMethodException | ScriptException e) {
			}
			
			graphics.render();
			
			keyboard.update();
			
			
			long dt = System.currentTimeMillis() - st;
			try {
				Thread.sleep(Math.max(0, 1000/60 - dt));
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
			
			fpsCount++;
			if (System.currentTimeMillis() - lastTime > 1000) {
				lastTime = System.currentTimeMillis();
				System.out.println("fps : " + fpsCount);
				fpsCount = 0;
			}
		}
	}
	
	public int[] getPalette() {
		return palette;
	}
	
	public void shutdown() {
		System.exit(0);
	}
}
