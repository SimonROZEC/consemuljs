import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException; 

public class Scripting { 

  public static void main(String args[]) throws ScriptException, NoSuchMethodException, InterruptedException, IOException { 
    int[] palette = new int[16];
    /*Random r = new Random();
    for (int i = 0; i < palette.length; i++) {
    	palette[i] = r.nextInt(0xffffff);
    }*/
    palette[0] = 0x000000;
    palette[1] = 0x1D2B53;
    palette[2] = 0x7E2553;
    palette[3] = 0x008751;
    palette[4] = 0xAB5236;
    palette[5] = 0x5F574F;
    palette[6] = 0xC2C3C7;
    palette[7] = 0xFFF1E8;
    palette[8] = 0xFF004D;
    palette[9] = 0xFFA300;
    palette[10] = 0xFFEC27;
    palette[11] = 0x00E436;
    palette[12] = 0x29ADFF;
    palette[13] = 0x83769C;
    palette[14] = 0xFF77A8;
    palette[15] = 0xFFCCAA;
    
    
 	Console window = new Console(256, 256, palette);
    new Thread(window).start();
  }  
}
  
  /*
  Name : Oracle Nashorn
Version : 10.0.1
Language name : ECMAScript
Language version : ECMA - 262 Edition 5.1
Extensions : [js]
Mime types : [application/javascript, application/ecmascript, text/javascript, text/ecmascript]
Names : [nashorn, Nashorn, js, JS, JavaScript, javascript, ECMAScript, ecmascript]
  */